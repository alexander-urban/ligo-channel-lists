#!/usr/bin/env python

"""Determine the difference between two instances of a given frametype
"""

from __future__ import print_function

import argparse
import csv
import gwdatafind
import json
import os.path
import subprocess
import sys
import warnings

from collections import OrderedDict

from gwpy.time import (from_gps, to_gps)

__author__ = 'Duncan Macleod <duncan.macleod@ligo.org>'


# -- utilities

def find_gwf(observatory, frametype, gps=None):
    if gps in [None, 'now']:
        gwf = gwdatafind.find_latest(observatory, frametype,
                                     on_missing='error')[0]
    else:
        gps = to_gps(gps)
        gwf = gwdatafind.find_urls(observatory, frametype, gps, gps,
                                   on_gaps='error')[0]
    return gwf.split('localhost')[-1]


def read_csv(filename):
    out = {}
    with open(filename, 'r') as f:
        reader = csv.reader(f)
        next(reader)  # skip header
        for name, rate in reader:
            yield name, float(rate)


def write_csv(chlist, filename):
    with open(filename, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(('name', 'rate'))
        for row in chlist:
            writer.writerow(row)

    return filename


# -- parse command line

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)

parser.add_argument('observatory')
parser.add_argument('frametype')
parser.add_argument('reference-file')
parser.add_argument('-r', '--replace', action='store_true', default=False,
                    help='replace the reference with the current channel '
                         'list when finished')
parser.add_argument('-t', '--gps', default='now',
                    help='GPS time to check')
parser.add_argument('-f', '--min-sample-rate', type=float,
                    default=0, help='minimum sample rate for channels')
parser.add_argument('--json', help='filepath for nagios JSON report')

args = parser.parse_args()
ref = os.path.abspath(os.path.expanduser(getattr(args, 'reference-file')))
minfs = args.min_sample_rate

# -- find frame and load current channels

channels = OrderedDict()

gwf = find_gwf(args.observatory, args.frametype, gps=args.gps)
frchan = subprocess.check_output('FrChannels {}'.format(gwf), shell=True)
for line in frchan.splitlines():
    name, rate = line.rstrip().decode().split(' ', 1)
    rate = float(rate)
    if rate >= minfs:
        channels[name] = [None, rate]

# -- load reference

if not os.path.isfile(ref):
    warnings.warn('Failed to locate {}, will create a new reference at '
                  'this location'.format(ref))
    write_csv(((key, value[1]) for key, value in channels.items()), ref)
    sys.exit(0)

for name, rate in read_csv(ref):
    try:
        channels[name][0] = rate
    except KeyError:
        channels[name] = [rate, None]

# -- create diff

added = []
removed = []
modified = []

for name in channels:
    old, new = channels[name]
    if old == new:
        continue
    if old is None:
        added.append(name)
    elif new is None:
        removed.append(name)
    else:
        modified.append((name, old, new))

# -- report

exitcode = 0
report = "No changes found"

if modified:
    print('----- The following channels sampling rate has changed')
    for name, old, new in modified:
        print('{:<50} {} -> {}'.format(name, old, new))
    exitcode = 1
    report = '{} channels have been resampled'.format(len(modified))

if added:
    print('---- The following channels are new')
    for name in added:
        print(name)
    exitcode = 1
    report = '{} channels have been added'.format(len(added))

if removed:
    print('---- The following channels have been removed')
    for name in removed:
        print(name)
    exitcode = 2
    report = '{} channels have been removed'.format(len(removed))

# -- write new reference

if args.replace:
    write_csv(((key, value[1]) for
               key, value in channels.items() if value[1]), ref)
    print('Current channel list written as reference to {}'.format(ref))

# write JSON report
if args.json:
    epilog = ('\nSee https://wiki.ligo.org/DetChar/ChannelListMonitoring for '
              'details on how to resolve problems')
    now = to_gps('now').gpsSeconds
    date = str(from_gps(now))
    jdict = OrderedDict([
        ('created_command', ' '.join(sys.argv)),
        ('created_datetime', date),
        ('created_gps', now),
        ('author', {'name': __author__.split('<')[0].strip(),
                    'email': __author__.split('<', 1)[1].rstrip('>')}),
        ('ok_txt', 'No changes in the channel list were detected'),
        ('unknown_txt', 'This monitor is not updating'),
        ('warning_txt', 'One more channels have been resampled, or are new'),
        ('critical_txt', 'One or more channels have been removed from '
                         'the channel list'),
        ('status_intervals', [
            {'num_status': exitcode,
             'start_sec': 0, 'end_sec': 7200,
             'txt_status': report + epilog,
            },
            {'num_status': 3, 'start_sec': 7200,
             'txt_status': 'This monitor is not updating',
            },
        ]),
        ('parameters', {
            'frametype': args.frametype,
            'observatory': args.observatory,
            'reference': ref,
        }),
        ('diff', {
            'rate': modified,
            'removed': removed,
            'added': added,
        }),
    ])
    with open(args.json, 'w') as f:
        json.dump(jdict, f)

sys.exit(exitcode)
